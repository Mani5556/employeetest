package com.example.employeeapp.retrofit;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EmployeeRetrofitClient {

    private static ApIService mApiService;

    public static synchronized ApIService getInstance() {
        if(mApiService == null){
            Retrofit retrofit = new Retrofit.Builder().
                    baseUrl("https://jsonplaceholder.typicode.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            mApiService = retrofit.create(ApIService.class);
        }
        return mApiService;
    }
}
