package com.example.employeeapp.ui.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.employeeapp.databinding.ActivityMainBinding;
import com.example.employeeapp.model.Employee;
import com.example.employeeapp.ui.detail.EmployeeDetailActivity;
import com.example.employeeapp.util.EmployeeViewModelFactory;
import com.example.employeeapp.util.Util;
import com.google.android.material.snackbar.Snackbar;

import java.time.Duration;


public class MainActivity extends AppCompatActivity implements EmployeeAdapter.OnClickListener {
    private ActivityMainBinding mBinding;
    private HomeViewModel mViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());
        setTitle("Employee List");
        mViewModel = ViewModelProviders.of(this,new EmployeeViewModelFactory()).get(HomeViewModel.class);
        mBinding.rvEmployeesList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL,false));

        mViewModel.employeesList.observe(this, response -> {
                switch(response.getStatus()){
                    case LOADING:
                        mBinding.progressbar.setVisibility(View.VISIBLE);
                        break;
                    case SUCCESS:
                        mBinding.progressbar.setVisibility(View.GONE);
                        mBinding.rvEmployeesList.setAdapter(new EmployeeAdapter(this,response.getData(),this));
                        break;
                    case ERROR:
                        mBinding.progressbar.setVisibility(View.GONE);
                        Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + response.getStatus());
                }
        });
        if(Util.isNetworkAvailable(this))
           mViewModel.getEmployees();
        else
            Snackbar.make(mBinding.getRoot(),"No internet Connection", Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(Employee employee) {
        Intent detailsIntent = new Intent(this, EmployeeDetailActivity.class);
        detailsIntent.putExtra("empID",employee.getId());
        startActivity(detailsIntent);
    }

    @Override
    public void onEmailClick(Employee employee) {
        startActivity(Intent.createChooser( Util.getEmailIntent(employee.getEmail()), "Send mail"));
    }
}