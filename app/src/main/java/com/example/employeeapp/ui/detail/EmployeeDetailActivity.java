package com.example.employeeapp.ui.detail;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.example.employeeapp.databinding.ActivityEmployeeDetailBinding;
import com.example.employeeapp.ui.home.HomeViewModel;
import com.example.employeeapp.util.EmployeeViewModelFactory;
import com.example.employeeapp.util.Util;
import com.google.android.material.snackbar.Snackbar;

public class EmployeeDetailActivity extends AppCompatActivity {
    private ActivityEmployeeDetailBinding mBinding;
    private HomeViewModel mViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ActivityEmployeeDetailBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setTitle("Employee Details");
        mViewModel = ViewModelProviders.of(this, new EmployeeViewModelFactory()).get(HomeViewModel.class);

        mViewModel.employeeObject.observe(this, response -> {
            switch (response.getStatus()) {
                case LOADING:
                    mBinding.progressbar.setVisibility(View.VISIBLE);
                    break;
                case SUCCESS:
                    mBinding.progressbar.setVisibility(View.GONE);
                    mBinding.setEmployee(response.getData());
                    mBinding.tvEmail.setOnClickListener(v->{
                        startActivity(Intent.createChooser( Util.getEmailIntent(response.getData().getEmail()), "Send mail"));
                    });

                    mBinding.tvPhone.setOnClickListener(v-> {
                        startActivity(Util.getPhoneIntent(response.getData().getPhone()));
                    });
                    break;
                case ERROR:
                    mBinding.progressbar.setVisibility(View.GONE);
                    Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + response.getStatus());
            }
        });



        mBinding.tvPhone.setOnClickListener(v->{

        });


        if(Util.isNetworkAvailable(this)) {
            int empId = getIntent().getIntExtra("empID", 1);
            mViewModel.getEmployeeById(empId);
        } else {
            Snackbar.make(mBinding.getRoot(),"No internet Connection", Snackbar.LENGTH_SHORT).show();
        }


    }

}